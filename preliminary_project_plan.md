# Preliminary project plan

## Better pitch

The measurement of expression profiles in large tissue regions, in this example in the brain of mice, using an automated image based assay like smFISH creates large datasets of terabytes size. To approach this problem, this project aims to implement the newest object instance segmentation method (Mask-RCNN) to build a pipeline where cell types and cell states can be identified (in this example using the RNA-expression levels of different markers). With this spatial information, one can differentiate more easily between healthy cells or cells with pathological conditions. This approach is very general and can be applied to diseases like cancer.

## Abstract

In this project, a new method for cell identification of smFISH-data is developed. During smFISH, the RNA-expression of cells in a tissue sample is observed. With the aid of fluorophor-markers, the fluorescence signal can be detected with a fluorescence microscope. As the output data is very large, computational image processing methods are indispensable. In the past, a watershed was performed, however, as the data grows continuously, its running time limits its feasibility. In this project, a pipeline is devoloped, which equips Mask R-CNN, a novel image processing program utilizing a convolutional neural network to extract RNA-expression data faster and more efficient.

## Month 1

Getting familiar with Mask R-CNN by testing the program with 1-2 examples to understand the code and the mechanism of action.

## Month 2-3

Creating a training set for the convolutional network. Tweaking the parameters, such as the size of the training set.

## Month 4-6

Establishing a functioning convolutional network and combining the CNN to the CARE network. Run on multiple datasets and finding the best parameters. 
