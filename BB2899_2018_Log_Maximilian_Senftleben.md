# Project Log Maximilian Senftleben

## 16.12.2018

Regarding the peer-reviewing of the report, I have some things, which I will add to the revised version of the report:

- maybe increase understanding of Methods to reproduce the code
- talk about the code, where was it used from (reference to github) and where is it accessable
- microscopy images with scale bars maybe

Will also do the revision of the report, which was assigned to me.


## 14.12.2018

After finishing all other important things (exams) I focus now on finishing the report. For that, I will try to create a boxplot summarizing all images from one run to see the average trend of confidence of the images. I am not sure if I can include that into the report, which is due to tomorrow, but I will include it into the final report.

## 07.12.2018

For the last week, I was mostly working on other courses, but still spend some time in Biomedicum. First, I tried to have a look on how to assess the coordinates, but that did not work as good as I thought. It might be exceeding the range of this project. 

What my supervisor and I tried to do was to assemble a new dataset of 1904 images or image-chunks to train the model on. Seemingly, something with the images is wrong and different to the other images. While training, the loss function does not go down as expected even at epoch>50. After the chunking of the 119 images (each image gets chunked into 16 smaller images stored as .npy file), I compared the shape of the numpy array per image to the old ones where it was functioning. But they were similar, thereforee, this cannot be the source of the failure. Now, I am trying to visualize the different layers of the numpy arrays showing for instance the masks to see if everything is correct there. Nevertheless, I fear that I cannot finish this, as the presentation is next week and therefore I have to focus more on preparing for that and writing the report.

## 30.11.2018

I managed now to normalize the graph even more (it still looks a bit off, as it is a barplot but okay). The image below was derived from smFISH of actual brain tissue, the nucleis are visibly harder to guess. The first image I uploaded on the 22.11. was derived from cell culture, where the nucleis are way easier to identify, both for the human eye and also for the model (same model was used as before).

![screenshot](https://bitbucket.org/maxsenh/BB2899/raw/master/segmented_tissue.png)

As one can see, some nuclei are recognized, most of them with a confidence score above 0.7. The objects classified as "undefined" reach a much lower score. One cluster was identified (lower left of the image) with a confidence score < 0.4. The model recognizes several objects multiple times slightly differently, so that the masks overlap (top left corner). To approach the problem, I could prohibit the model to assign objects if its area matches about 60-70% of a previously assigned object. This would include modifying the code and compute the area of nucleis with the given coordinates per objects. Additionally, some of the objects identified by the model contain multiple nucleis or somehow cover huge areas, in which no nuclei/undefined/cluster exists. After I found out, how the coordinates of the objects are stored, I could set a size-threshold of the object area. Setting this threshold can be done in two ways. I could use the mean size of the respective classes from the training data. The downside of this approach is, that it will be biased from the training data, contradicting the idea of having a flexible model, which is able to identify the classes with possible variation in size. Another approach is to run the segmentation for the dataset and get the mean of the respective classes after the segmentation and then remove the objects which are outside of the area-threshold. This would require more computation time.

## 29.11.2018

I could generate a nice PDF for all the output images plus histograms. The next step is to do a pooled boxplot as I think it is the best representation for all the images.

## 22.11.2018

I am still working on the script to generate a pdf gathering results. It is just very labor-intensive, as the only python library I found useful was reportlab, which comes with a 115-pages userguide...
Will be useful for the future though!

### New try to show results

![screenshot](https://bitbucket.org/maxsenh/BB2899/raw/master/result_segmented.png)

Finally, I could figure out how to input pictures on bitbucket! It worked easily with github and now also on bitbucket. 
As one can see, most of the nuclei occur as single nuclei and are identified through segmentation. The training was done with 160 images and epoch=50. 
Each object is marked with a box and a mask and is described with the type of object (in this case it is only nucleis, but remember that there is two other classes: undefined and cluster). Mask R-CNN uses different colors to make it easier to distinguish between objects.
Nevertheless, one can also see the limitations: 

- lack of distinguishing between two overlapping nuclei
- confusion when cluster occur (on the top, green and purple gets mixed up)

## 19.11.2018

I am back in the lab to work further on my project after I had to prepare a presentation and an exam. After going through some images, I will now gather result images into a pdf, where I will input the picture, a distribution graph of confidence scores per picture, the scores, the image ID and a pooled graph showing all imgages together.

Next, I will have to continue to label images.

Unfortunately, I could not upload any result images so far, because I dont know how to upload.

## 14.11.2018

My supervisor run our dataset (160 images in total) on the google cloud, because I do not have access yet and gave me the result files and the dataset. I will now do the inference on my local computer to see how it performs. For that I will create an environment, in which mrcnn and tensorflow and stuff is installed.

For the evaluation: gcloud_training/validation_test, test segmentation images in Nuclei-Test.zip
also: check GPU-count and count of images per GPU

![](https://bitbucket.org/maxsenh/bb2899/src/master/result_segmented.png)


## 11.11.2018

While copying the files in the large_data folder, I encounter that all of the labeled pictures from labelbox are related to the "Nuclei_SN_Hyb2" experiment. Secondly, there are two SN_hyb2 folders in the large_folder, one where SN is written in small letters. Whatever that means. I am now only searching in the nuclei_sn_hyb2 folder and uppercase all the letters. Writing this down, so that I can tell my supervisor.

The labelled pictures are in ~/Project_mrcnn/large_data_extracted/. Task 1 done.

I was trying to do some inference according to task 2, but someone is running something on hinton. Will do labeling instead.

## 07.11.2018

Three things to do:
1. parse the bigger dataset with the script "Training 20180618_code_datalab_checkpoint.ipynb" and the subpart functions (the first one) from Simones github. The files are in the dropbox under "Large_dataset". Fetching the names first, then building the dataset. Use python module json for that to read files.

2. play around with the last part of the mrcnn-code, which deals with inference and so on. See how images look like
3. do some labeling of new images on labelbox.io

maybe:
4. evaluate the big run with 160 images, chunk=4 and epoch=50 for the head layers and find best epoch. Then test the sublayers.

## 06.11.2018

Fortunately, Simone is back from his trip to the US and he helped me to clarify some questions about the project. The cells being visible on the images I received were the nucleis of brain cells. The white spots in the nucleis are heterochromatins (condensed DNA). In the following, there is a better, more broader, picture of the cells and their origin.

![](https://media.springernature.com/lw900/springer-static/esm/art%3A10.1038%2Fs41592-018-0175-z/MediaObjects/41592_2018_175_Fig5_ESM.jpg)

As Tensorboard is very good to evaluate the loss function at different epochs (even in real-time) it is not necessary to iterate. You just have to run it with a high epoch number (something like epoch=50) and look on the loss function. The lowest point of the courve is the best possible epoch to be used. I will do that on training the head layers, while leaving the rest of the model at epoch=1, finding the best suitable epoch for the heads, and then switch to the rest of the model. Tomorrow I will go and use google if Simone managed to set it up. Chunk = 4, 10 images, which means 160 images.


## 02.11.2018

The solution for the below mentioned problem is, that the models reserved a lot of space on the root directory. In fact, there was no space left for simple linux-tasks like TAB auto-complete. So I simply removed old unused models and had enough space again. 
Next, I did some running of the model, but rather focused on tensorboard. In the code, there are several evaluation parameters programmed, which are visualized in tensorboard after running. So I am doing literature research about those.

## 27.10.2018

### encountered problems, error told not enough space left on device...possibly temporary files??

To visualize multiple run simply do:
```
tensorboard --logdir=name1:path1,name2:path2
```

- API - application programming interface,  set of clearly defined methods of communication among various components.
- loss function - is a function that maps an event or values of one or more variables onto a real number intuitively representing some "cost" associated with the event, it basically means that the closer the function gets to 0, the better the prediction is. Each datapoint in a loss function is a different epoch
- placeholders, they are like variables, they are used for feeding the data from the outside
- Faster RCNN -> object detection, bounding labels
- Mask -> labels each pixel if its part of the object or not

## 26.10.2018

Back to business! Tensorboard is a good tool to evaluate the training as it creates learning graphs. I will show these learning graphs on the half-time presentation.

```
# opens the directory in tensorboard (should + does)
tensorboard --logdir path/to/logs/directory
```
Yes it does show me an output! What I see is the scalars and graphs for the training of the head-branches for an example. In the directory specified, the events are saved as .h5 files and tensorboard is monitoring them in real time, as it refreshes every few seconds. Now it should not be a problem to do some iteration with monitoring at the same time. Furthermore, I really want to try tensorboard/flow with own examples, this is really interesting! Will set that up on my own computer.

### Something important to mention

One should close Tensorboard with Ctrl + C BEFORE closing the tab in the browser (don´t ask me why).

## 22.10.2018

The exam time is really hard for me as I have to study really hard. Therefore, I did not have so much time to spend on the project. 
I mostly used the time to read up on Mask R-CNN, Keras, Tensorboard and deep learning in general. I have the feeling, that I am just scratching the surface while not understanding fully everything. Tomorrow, I will go to the lab and meet Simone and ask him some questions.


## 14.10.2018

For a reason, there is a memory build-up somewhere in the code. Apparently, the training creates data which is not affilicated with the model and which does not remove itself after the training. Nevertheless, a way to account for this is to include a code snippet which cleans the memory explicitely after each epoch. I will try this next week. 
I will run epochs like 1, 5, 10 both for coco and image_net.

To evaluate the results generated, I will download the files from dropbox and evaluate them with tensorboard. The files are in Mask_RCNN/model_nuclei/nuclei... in Simone's dropbox.

## 04.10.2018

The error below was due to the wrong setting for GPU. I thought, that the cluster computer has more than one graphic card, but that is wrong. It has only one (Nvidia Titan X).

Testing parameters are:

- Epoch (complete presentation of the data set to a learning machine, they usually need more than one, one is enough for testing)
- RPN-value (region proposal network, see below), Non-max suppression threshold to filter RPN proposals. You can increase this during training to generate more propsals.
- Steps of Epoch?
- Validation steps

### How does RPN work?
1. image goes through a convolutional network, which outputs a set of convolutional feature maps on the last convolutional layer

2. SLiding window is run spatially on these future maps (n x n). Each sliding window has set of 9 anchors, all have the same anchor, but with 3 different aspect ratios and 3 different scales as shown below.

3. 3. Finally, the 3×3 spatial features extracted from those convolution feature maps (shown above within red box) are fed to a smaller network which has two tasks: classification (cls) and regression (reg). The output of regressor determines a a predicted bounding-box (𝑥,𝑦,𝑤,ℎ), The output of classification sub-network is a probability 𝑝 indicating whether the the predicted box contains an object (1) or it is from background (0 for no object).

## 01.10.2018

Having the possibility to run the large computer is nice, but only if it works. I got the following error.

```
RuntimeError: It looks like you are subclassing `Model` and you forgot to call `super(YourClass, self).__init__()`. Always start with this line.
```

Research has not been helpful so far, I will ask tomorrow. There are various options, such as image folder too large (which would be weird), wrong path, wrong annotation file (also weird) or wrong chunking. I simply dont know.

## Better pitch

The measurement of expression profiles in large tissue regions, in this example in the brain of mice, using an automated image based assay like smFISH creates large datasets of terabytes size. To approach this problem, this project aims to implement the newest object instance segmentation method (Mask-RCNN) to build a pipeline where cell types and cell states can be identified (in this example using the RNA-expression levels of different markers). With this spatial information, one can differentiate more easily between healthy cells or cells with pathological conditions. This approach is very general and can be applied to diseases like cancer.

## 30.09.2018

After consulting my supervisor, I am now ready to run my program on a machine, which is more appropriate: Deploying hundreds of cores together with multiple GPUs seems to be more efficient and will reduce the running time to a fraction. Having this, I will also try different sizes of datasets and iterate through several parameters. However, I will have to cut down the time I am investing, I have a couple of other assignments for other courses waiting to be finished.

## 27.09.2018

After finally understanding the code of the Mask-RCNN, I started to run the pipeline using a dataset of ~10 images, which I separated into training data and validation data. Although the data set does not seem to be big, something did not quite as planned, leaving the computer freezing and computing for a long time. Reducing the data set even more did not help. I will have to ask my supervisor to help me, as I think that there might be bug in the code.

Furthermore, I got some warnings during running.


```/home/max/.local/lib/python3.6/site-packages/tensorflow/python/ops/gradients_impl.py:108: UserWarning: Converting sparse IndexedSlices to a dense Tensor of unknown shape. This may consume a large amount of memory.
  "Converting sparse IndexedSlices to a dense Tensor of unknown shape. "

/home/max/anaconda3/lib/python3.6/site-packages/keras/engine/training_generator.py:46: UserWarning: Using a generator with `use_multiprocessing=True` and multiple workers may duplicate your data. Please consider using the`keras.utils.Sequence class.
  UserWarning('Using a generator with `use_multiprocessing=True`'
```

## 24.09.2018

In the last week, I had to look more into the code, as I realised that I am missing quite a lot of skills. For example, I have never worked with python classes before. Neither was I working with Tensorflow as a environment for machine learning. My supervisor is very helpful, as he provides me with all the help I need. This week, I should be able to start a training process with the model built by my supervisor. After that, I can finally set the parameters to the optimal.

## 12.09.2018

I realised, that I forgot to post something in this log last week and I am really sorry about that. The main reason for that was most likely, that I could not work as much as I desired on the project, as my computer broke and my supervisor and I tried to fix a computer in the lab for me. 

Nevertheless, I sat together with my project partner Yuanyuan and we distributed our project. Subsequently, we were writing our preliminary project plan. Yuanyuan will deal with the raw data output of the smFISH-application and increase the signal for each dot in the image. In the following, I will take Yuanyuan�s output data and apply the Mask R-CNN program on it. I have read through the code so far and will sit down with my supervisor tomorrow to finally start coding.

In my next log entry (which I will make on this weekend) I will write about my impressions about the code and about my progress.

## 30.08.2018

### My expectations about this course

I chose the project about optimization of smFISH when dealing with big datasets, because I am convinced that it will require my full attention thorought the project. Furthermore, I am happy to experience new programming techniques, which will be a perfect preparation for not only my master thesis, but also for my future scientific career in bioinformatics. Besides programming skills, I hope to get a deeper insight into the scientific daily life, as I will be exposed to handling a lot of trail-and-error and, even more important, to experience well-deserved success.
