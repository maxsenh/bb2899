 ### some general stuff:
 - .ipynb is an ending for a python notebook, which can be converted to a python file with the following command:
 
```bash
ipython nbconvert --to python <YourNotebook>.ipynb
```

for python classes:
it is apparently something like a collection of functions.

__init__ is the initiation function for a class, its always called in the beginning of a class. its always executed when the class is initiated

objects can contain methods, which are functions belonging to the object
"self" parameter references to the class instance itself, is used to access variables belonging to the class

"self" has to be the first parameter for each function in the class
its basically the first parameter in every function disregarding the name (doesnt have to be self)

