# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 13:22:06 2018

@author: max
"""

import json
import os
import sys
from os.path import expanduser

home = expanduser("~")
project_folder = home + "/Project_mrcnn/"
code_folder = home + "/git/bb2899/code/"

def parse_json(json_file,annotation_dir,image_size):

    loaded=json.load(open(json_file))    
    list_of_files=[one_annotation["External ID"] for one_annotation in loaded]
    list_of_dir=[one_annotation["Dataset Name"] for one_annotation in loaded]
    
    [print('~/Project_mrcnn/large_data/Nuclei_SN_Hyb2/Nuclei_SN_Hyb2_to_label/'+list_of_files[i]) for i in range(len(list_of_files))]

    
parse_json(project_folder+"All_labels_20181107.json",project_folder,512)
