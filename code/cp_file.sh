list_of_files=$( python parse_jsonfile.py)

getIDonly() {
    echo $1 | cut -d : -f 1 | xargs basename
}

for full_path in $list_of_files
    do

    only_ID=$( getIDonly $full_path )
    
    echo $only_ID
    cp ~/Project_mrcnn/large_data/Nuclei_SN_Hyb2/Nuclei_SN_Hyb2_to_label/$only_ID ~/Project_mrcnn/large_data_extracted/$only_ID

done